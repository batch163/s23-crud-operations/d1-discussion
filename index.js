
/*
	CRUD OPERATIONS
*/

	/*
		Create Operation
			- add/insert new documents to a collection.

			syntax:

			db.collections.insertOne(document)
			db.collections.insertMany([documents])
	*/

		//Example: We will insert one document

		db.users.insertOne(
			{
				"firstName": "Jane",
				"lastName": "Doe",
				"age": 21,
				"contact": {
					"phone": "87654321",
					"email": "janedoe@gmail.com"
				},
				"courses": ["CSS", "Javascript", "Python"],
				"department": "none"
			}
		)


		//Example: We will insert 2 documents

		db.users.insertMany(
			[
				{
					"firstName": "Stephen",
					"lastName": "Hawking",
					"age": 76,
					"contact": {
						"phone": "87654321",
						"email": "stephenhawking@gmail.com"
					},
					"courses": ["Python", "React", "PHP"],
					"department": "none"
				},
				{
					"firstName": "Neil",
					"lastName": "Armstrong",
					"age": 82,
					"contact": {
						"phone": "87654321",
						"email": "neilarmstrong@gmail.com"
					},
					"courses": ["React", "Laravel", "Sass"],
					"department": "none"
				}
			]
		)


		/*
			Read Operation
				- retrieve documents from a collection;


			syntax:

				db.collections.find(query, projection)
		*/

			db.users.find()



		/*
			Update Operation
				- modify existing documents in a collection.


			syntax:

				db.collections.updateOne(filter, update)
				db.collections.updateMany(filter, update)
				db.collections.replaceOne()
		*/

		//we will insert this document as an example to the updateOne() method
		db.users.insertOne({
			"firstName": "Test",
			"lastName": "Test",
			"age": 0,
			"contact": {
				"phone": "0",
				"email": "test"
			},
			"courses": [],
			"department": "test"
		})



		// use the updateOne() method to this example
		db.users.updateOne(
			{
				"firstName": "Test"
			},
			{ 
				$set: { 
					"firstName": "Bill",
					"lastName": "Gates",
					 "age": 65,
					 "contact":{
					 	"phone": "12345678",
					 	"email": "bill@gmail.com"
					 },
					 "courses": ["PHP", "Laravel", "HTML"],
					 "department": "Operations",
					 "status": "active"
				}
			}
		)



		//setting a value/adding non existing field
		db.users.updateOne(
			{
				"_id" : ObjectId("621f2c220ada95273a69f740")
			},
			{
				$set: {
					"isAdmin": true
				}
			}
		)

		
		//remove existing field
		db.users.updateOne(
			{
				"_id" : ObjectId("621f2c220ada95273a69f740")
			},
			{
				$unset: {
					"isAdmin": true
				}
			}
		)



		//UPDATE MULTIPLE DOCUMENTS
			//update department to HR
		db.users.updateMany({"department": "none"}, {$set: {"department": "HR"}})


		//update HR department with status active field
		db.users.updateMany({"department": "HR"}, {$set: {"active": "status"} })


		//if using updateOne method, if you update HR department documents with isAdmin field to false, what do you think will happened?
		db.users.updateOne({"department": "HR"}, {$set: {"isAdmin": false} })
			//this will only update the first document that passes the filter document, not the entire documents


		// updateOne vs. replaceOne

			//using updateOne method, update the first name field to your first name
			db.users.updateOne({"firstName": "Test"}, {$set: {"firstName": "Joy"}})
				//updates the field/s specified in the update document parameter while keeping all the existing fields

			//using replaceOne method, update the last name field with your own last name
			db.users.replaceOne({"lastName": "Test"}, {"lastName": "Pague"})
				//replace the whole document with the replacement document parameter


		// DELETE METHOD
			//  - Removes a single document from a collection.

			// db.collections.deleteOne({filter})
			// db.collections.deleteMany({filter})


		// deleteOne
		db.users.deleteOne(
			{"_id" : ObjectId("62200f4f7186cecd1088e8c3") }
		)



		//deleteMany
			// Removes all documents that match the filter from a collection.

		//create dummy documents for deleteMany method
		db.users.insertMany(
			[
				{
					"course": "JS",
					"price": 1000
				},
				{
					"course": "JAVA",
					"price": 3000
				},
				{
					"course": "PHP",
					"price": 5000
				}
			]
		)


		db.users.deleteMany(
			{ "price": { $gte: 3000 } }
		)


		// insert dummy documents
		db.users.insertMany(
			[
				{
					"course": "JAVA",
					"price": 3000
				},
				{
					"course": "PHP",
					"price": 5000
				}
			]
		)


		//use $in operator to target multiple documents of the same field
		db.users.deleteMany(
			{
				"_id": { 
					$in: [
						ObjectId("622013a07186cecd1088e8c4"),
						ObjectId("622015967186cecd1088e8c7")
					]
				}
			}
		)
